package com.app.weather.list;

import com.app.weather.Country;
import com.app.weather.R;
import com.app.weather.R.id;
import com.app.weather.R.layout;
import com.app.weather.R.menu;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;

public class ItemDetailActivity extends FragmentActivity {
	ItemDetailFragment fragmentItemDetail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_detail);
		String city = (String) getIntent().getSerializableExtra("city");
		String data = (String) getIntent().getSerializableExtra("data");
		System.out.println("Data data2 : " + data);
		System.out.println("city city2 : " + city);
		if (savedInstanceState == null) {
			fragmentItemDetail = ItemDetailFragment.newInstance(city,data);
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace(R.id.flDetailContainer, fragmentItemDetail);
			ft.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.item_detail, menu);
		return true;
	}

}