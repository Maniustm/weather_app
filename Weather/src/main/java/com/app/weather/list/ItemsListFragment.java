package com.app.weather.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.app.weather.Adapter.CustExpListview;
import com.app.weather.Continent;
import com.app.weather.Country;
import com.app.weather.Adapter;
import com.app.weather.Adapter.SecondLevelAdapter;
import com.app.weather.R;
import com.app.weather.R.id;
import com.app.weather.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//klasa aduje podstawowe dane , laczy listner z widokiem  i adapterem

public class ItemsListFragment extends Fragment implements OnChildClickListener {

	public OnItemSelectedListener listener;
	private static List<Continent> Continents;
	private ExpandableListView expandableListView;
	private Adapter cuntryAdapter;

	public interface OnItemSelectedListener {
		public void onItemSelected(String i);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnItemSelectedListener) {
			listener = (OnItemSelectedListener) activity;
		} else {
			throw new ClassCastException(
					activity.toString()
							+ " must implement ItemsListFragment.OnItemSelectedListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_items_list, container,
				false);

		LoadContinents();

		expandableListView = (ExpandableListView) view
				.findViewById(R.id.expandableListView);
		cuntryAdapter = new Adapter(view.getContext(), Continents, this);
		expandableListView.setAdapter(cuntryAdapter);

		return view;
	}

	private void LoadContinents() {

		Continents = new ArrayList<Continent>();

		List<Country> Asia = new ArrayList<Country>();
		List<Country> Australia = new ArrayList<Country>();
		List<Country> Europe = new ArrayList<Country>();

		ArrayList<String> citiesChina = new ArrayList<String>(Arrays.asList(
				"Hong Kong", "Shanghai", "Beijing"));
		Asia.add(new Country("China", citiesChina));
		ArrayList<String> citiesIndia = new ArrayList<String>(Arrays.asList(
				"Bombay", "Calcutta", "Delhi", "Madras"));
		Asia.add(new Country("India", citiesIndia));
		Continents.add(new Continent("Asia", Asia));

		ArrayList<String> citiesAustralia = new ArrayList<String>(
				Arrays.asList("Brisbane", "Hobart", "Melbourne", "Sydney"));
		Australia.add(new Country("Australia", citiesAustralia));
		ArrayList<String> citiesNewZealand = new ArrayList<String>(
				Arrays.asList("Auckland", "Christchurch", "Wellington"));
		Australia.add(new Country("New Zealand", citiesNewZealand));
		Continents.add(new Continent("Australia", Australia));

		ArrayList<String> citiesRussia = new ArrayList<String>(Arrays.asList(
				"Moscow", "Kursk", "Novosibirsk", "Saint Petersburg"));
		Europe.add(new Country("Russia", citiesRussia));
		ArrayList<String> citiesEngland = new ArrayList<String>(Arrays.asList(
				"London", "Oxford"));
		Europe.add(new Country("England", citiesEngland));
		Continents.add(new Continent("Europe", Europe));
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {

		listener.onItemSelected(((TextView) v).getText().toString());

		Toast.makeText(v.getContext(), ((TextView) v).getText(),
				Toast.LENGTH_SHORT).show();

		return false;
	}
}