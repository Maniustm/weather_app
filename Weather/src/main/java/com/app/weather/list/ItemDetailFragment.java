package com.app.weather.list;

import java.io.FileOutputStream;
import org.json.JSONException;

import com.app.weather.R;
import com.app.weather.json.JsonParser;
import com.app.weather.json.Weather;
import com.app.weather.json.WeatherHttpClient;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemDetailFragment extends Fragment {
	private String city;
	private String data;
	private TextView cityText;
	private TextView condDescr;
	private TextView temp;
	private TextView press;
	private TextView windSpeed;
	private TextView windDeg;
	private TextView hum;
	private ImageView imgView;
	Bitmap bm = null;
	private static final String PREFS_NAME = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		city = (String) getArguments().getSerializable("city");
		data = (String) getArguments().getSerializable("data");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_item_detail, container,
				false);

		cityText = (TextView) view.findViewById(R.id.cityText);
		condDescr = (TextView) view.findViewById(R.id.condDescr);
		temp = (TextView) view.findViewById(R.id.temp);
		hum = (TextView) view.findViewById(R.id.hum);
		press = (TextView) view.findViewById(R.id.press);
		windSpeed = (TextView) view.findViewById(R.id.windSpeed);
		windDeg = (TextView) view.findViewById(R.id.windDeg);
		imgView = (ImageView) view.findViewById(R.id.condIcon);

		JSONWeatherTask task = new JSONWeatherTask();
		task.execute(new String[] { city, data });

		return view;
	}

	public static ItemDetailFragment newInstance(String item1, String item2) {
		ItemDetailFragment fragmentDemo = new ItemDetailFragment();
		Bundle args = new Bundle();
		args.putSerializable("city", item1);
		args.putSerializable("data", item2);
		System.out.println("Data item2 : " + item2);
		fragmentDemo.setArguments(args);
		return fragmentDemo;
	}

	private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {

		private ConnectivityManager connManager;
		private String imagefilename = "imagePogoda.gif";

		@Override
		protected Weather doInBackground(String... params) {
			connManager = (ConnectivityManager) getActivity().getSystemService(
					Context.CONNECTIVITY_SERVICE);

			Weather weather = new Weather();
			data = params[1];

			if (data == null) {
				if (isConnection() == false) {
					getActivity().setContentView(R.layout.activity_main);
					System.out.println("brak danych, brak polaczenia");
				} else
					data = ((new WeatherHttpClient()).getWeatherData(params[0]));
			}

			if (data != null) {
				try {
					
					//pobieranie danych pogody
					weather = JsonParser.getWeather(data);

					// pobieranie ikony
					if (isConnection() == true) {
						weather.iconData = ((new WeatherHttpClient())
								.getImage(weather.currentCondition.getIcon()));
//						System.out.println("Kod ikony"+ weather.currentCondition.getIcon());
						saveImage(weather.iconData, imagefilename);
					} else {
						weather.iconData = null;
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return weather;

		}

		private boolean isConnection() {
			if (connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
					.isConnected()
					|| connManager
							.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
							.isConnected()) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Weather weather) {
			super.onPostExecute(weather);

			if (weather.iconData != null && weather.iconData.length > 0) {

				Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0,
						weather.iconData.length);
				imgView.setImageBitmap(img);

			} else {
				imgView.setImageDrawable(getImgFromFile("/" + imagefilename,
						getActivity().getBaseContext()));
			}

			cityText.setText(weather.location.getCity() + ","
					+ weather.location.getCountry());
			condDescr.setText(weather.currentCondition.getCondition() + "("
					+ weather.currentCondition.getDescr() + ")");
			temp.setText(""
					+ Math.round((weather.temperature.getTemp() - 273.15))
					+ " C");
			hum.setText("" + weather.currentCondition.getHumidity() + "%");
			press.setText("" + weather.currentCondition.getPressure() + " hPa");
			windSpeed.setText("" + weather.wind.getSpeed() + " mps");
			windDeg.setText("" + weather.wind.getDeg() + "�");

			saveInMemory(weather.location.getCity(), data);
		}

		public Drawable getImgFromFile(String filename, Context context) {
			Log.w("file", "obrazek wczytany " + context.getFilesDir()
					+ filename);
			return Drawable.createFromPath(context.getFilesDir() + filename);
		}

		public boolean saveImage(byte[] img, String filename) {
			try {
				FileOutputStream fileImg = getActivity().openFileOutput(
						filename, Context.MODE_PRIVATE);
				fileImg.write(img);

			} catch (Exception e) {
				Log.d("file", " błąd zapisu");
				return false;
			}
			return true;
		}

		public void saveInMemory(String cityName, String cityData) {
			System.out.println("saveCityInMemory: " + cityName);
			SharedPreferences settings = getActivity().getSharedPreferences(
					PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("cityName", cityName);
			editor.putString("cityData", cityData);
			editor.commit();

		}
	}
}