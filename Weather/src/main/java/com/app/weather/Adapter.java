package com.app.weather;

import java.util.List;

import android.R;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class Adapter extends BaseExpandableListAdapter {
	public List<Continent> continents;
	public LayoutInflater inflater;
	public List<Country> countries;
	public CustExpListview SecondLevelexplv;
	public ExpandableListView.OnChildClickListener childLst;

	public Adapter(Context context, List<Continent> continents,
			ExpandableListView.OnChildClickListener childLst) {
		this.continents = continents;
		inflater = LayoutInflater.from(context);
		this.childLst = childLst;

	}

	public Adapter() {
	}

	@Override
	public int getGroupCount() {
		return continents.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return continents.get(groupPosition).getCountries().size() / 2;
	}


	@Override
	public Object getGroup(int groupPosition) {
		return continents.get(groupPosition).getName();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return continents.get(groupPosition).getCountries().get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(
					R.layout.simple_expandable_list_item_1, parent, false);
		}

		((TextView) convertView).setText(getGroup(groupPosition).toString());
		convertView.setBackgroundColor(Color.GRAY);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		SecondLevelexplv = new CustExpListview(parent.getContext());
		SecondLevelexplv.setAdapter(new SecondLevelAdapter(groupPosition));
		SecondLevelexplv.setOnChildClickListener(childLst);
		SecondLevelexplv.setGroupIndicator(null);

		return SecondLevelexplv;
	}

	@Override
	public boolean isChildSelectable(int i, int i2) {
		return true;
	}

	public class CustExpListview extends ExpandableListView {

		int intGroupPosition, intChildPosition, intGroupid;

		public CustExpListview(Context context) {
			super(context);
		}

		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			widthMeasureSpec = MeasureSpec.makeMeasureSpec(960,
					MeasureSpec.AT_MOST);
			heightMeasureSpec = MeasureSpec.makeMeasureSpec(600,
					MeasureSpec.AT_MOST);
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}

	public class SecondLevelAdapter extends BaseExpandableListAdapter {
		int chosenContinent;

		public SecondLevelAdapter(int groupPosition) {
			chosenContinent = groupPosition;

		}

		public SecondLevelAdapter(Context context, List<Continent> continents) {

		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {

			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.simple_list_item_1,
						parent, false);
			}

			((TextView) convertView).setText(getChild(groupPosition,
					childPosition).toString());

			return convertView;

		}

		@Override
		public int getGroupCount() {
			return continents.get(chosenContinent).getCountries().size();
		}

		@Override
		public int getChildrenCount(int groupPosition) {

			return continents.get(chosenContinent).getCountries()
					.get(groupPosition).getCities().size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return continents.get(chosenContinent).getCountries()
					.get(groupPosition).getName();
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return continents.get(chosenContinent).getCountries()
					.get(groupPosition).getCities().get(childPosition);
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.simple_expandable_list_item_1, parent, false);
			}

			((TextView) convertView)
					.setText(getGroup(groupPosition).toString());

			return convertView;

		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

	}

}