package com.app.weather;

import java.io.Serializable;
import java.util.List;

public class Country implements Serializable{
	  private String Name;
	  private List Cities;

	  public Country(String name, List cities) {
	    Name = name;
	    Cities = cities;
	  }

	  public String getName() {
	    return Name;
	  }

	  public List getCities() {
	    return Cities;
	  }
	  
	}