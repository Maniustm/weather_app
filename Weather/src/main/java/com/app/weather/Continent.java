package com.app.weather;

import java.util.List;

public class Continent {
	
	public List<Country> countries;
	public String name;

	public Continent(String name, List<Country> countries) {		
		this.countries = countries;
		this.name = name;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
