package com.app.weather;

import com.app.weather.list.ItemDetailActivity;
import com.app.weather.list.ItemsListFragment.OnItemSelectedListener;

import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.Menu;

public class MainActivity extends FragmentActivity implements
		OnItemSelectedListener {

	private ConnectivityManager connManager;
	static boolean polaczony;
	private static final String PREFS_NAME = null;
	String cityName;
	String cityData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_items_list);

		String networkService = Context.CONNECTIVITY_SERVICE;
		connManager = (ConnectivityManager) getSystemService(networkService);
		
		checkState();
	}

	public void checkState() {
		loadFromMemory();
		if (isNetwork()) {
			System.out.println(" jest połączenie");

			if (cityData != null && cityName != null) {
				showDetailsActivity(cityName, null);
			}
		} else if (!isNetwork()) {
			System.out.println("brak poczenia");

			if (cityData != null) {
				showDetailsActivity(cityName, cityData);
			} else {
				setContentView(R.layout.activity_main);
				System.out.println(" brak danych w pamieci");
			}
		}

	}

	private boolean isNetwork() {
		System.out.println(" sprawdzam połączenie");
		if (connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.isConnected()
				|| connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
						.isConnected()) {
			polaczony = true;
			System.out.println(" jest połączenie");
		} else {
			polaczony = false;
			System.out.println("brak poczenia");
		}
		return polaczony;
	}

	@Override
	public void onItemSelected(String city) {

		this.cityName = city;

		

		if (isNetwork()) {
			showDetailsActivity(city, null);
		} else {
			setContentView(R.layout.activity_main);
		}

	}

	private void showDetailsActivity(String city, String data) {
		Intent i = new Intent(this, ItemDetailActivity.class);
		i.putExtra("city", city);
		i.putExtra("data", data);
		startActivity(i);
	}

	public void loadFromMemory() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		cityName = settings.getString("cityName", cityName);
		cityData = settings.getString("cityData", cityData);
		// System.out.println("loadCityFromMemory: " + cityName);
		// System.out.println("loadDataFromMemory: " + cityData);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.items, menu);
		return true;
	}

}
